class CreateGames < ActiveRecord::Migration
  def change
    create_table :games do |t|
      t.belongs_to :home_player, index: true
      t.belongs_to :away_player, index: true
      t.integer :home_score
      t.integer :away_score

      t.timestamps
    end
  end
end
