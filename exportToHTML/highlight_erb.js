$(function(){
    var ERB_TAG_SELECTOR = '.s3'
    var RUBY_CODE_FILTER_REGEXP = /[^a-zA-Z@\s\._|\(\)]/

    function appendRubyCodeCss() {
        var $style = $('<style>')
        var cssRules = []
        cssRules.push('.ruby-code { background-color: rgba(0,200,0,0.1) }')
        cssRules.push('.ruby-code.ruby-code-dirty { background-color: lightcoral }')
        $style.html(cssRules.join("\n"))
        $('head').append($style)
    }

    function styleRubyCode() {
        var $openingErbTags = $('.s3:contains("<")')
        $openingErbTags.each(function(){
            var $openingTag = $(this)
            var $closingTag = $openingTag.nextAll(ERB_TAG_SELECTOR).eq(0)
            var $rubyCode = $openingTag.nextUntil(ERB_TAG_SELECTOR)
            var rubyCodeWithoutTags = $rubyCode.text()
            $rubyCode = $rubyCode.add($openingTag)
            $rubyCode = $rubyCode.add($closingTag)
            $rubyCode.wrapAll('<span class="' + rubyCodeCssClass(rubyCodeWithoutTags) + '"></span>')
        })
    }

    function rubyCodeCssClass(rubyCode) {
        var cssClass
        if(rubyCode.match(RUBY_CODE_FILTER_REGEXP)) {
            console.log(rubyCode.match(RUBY_CODE_FILTER_REGEXP))
            cssClass = 'ruby-code ruby-code-dirty'
        } else {
            console.log('clean')
            cssClass = 'ruby-code'
        }
        return cssClass
    }

    function bindings() {
        var $infoDiv = $('button')
        var originalText = $infoDiv.text()
        $('.ruby-code-dirty').on('mouseover', function(){
            var code = $(this).text()
            code = code.replace('<%=', '')
            code = code.replace('<%', '')
            code = code.replace('%>', '')
            $infoDiv.text("What's wrong: " + code.match(RUBY_CODE_FILTER_REGEXP))
        })
        $('.ruby-code-dirty').on('mouseout', function(){
            $infoDiv.text(originalText)
        })
    }

    function appendButton() {
        var $button = $('<button>')
        $button.text('Go!')
        $button.css({position: 'fixed', top: '50px', right: '20px', height: '40px', width: '180px'})
        $button.click(function(){
            appendRubyCodeCss()
            styleRubyCode()
            bindings()
        })
        $('pre').prepend($button)
    }

    appendButton()
})