json.array!(@games) do |game|
  json.extract! game, :id, :home_player_id, :away_player_id, :home_score, :away_score
  json.url game_url(game, format: :json)
end
